import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Parser {

	/** String constant for the encoding scheme. */
	private static final String PLACE_HOLDER = "##";
	private static final String BASE_PATH = ".wikipedia.org";
	private static final String COUNTRY_CODE_GERMANY = "de";
	private static final String PROTOCOL = "http://";
	private Map<Integer, String> articles = new HashMap<Integer, String>();
	private Map<Integer, String> prepositions = new HashMap<Integer, String>();
	private Map<Integer, String> wordsToRemove;
	private WordClass wordClass;
	private String buf;

	public Parser() {
		initSets();
	}

	public String processSite(String url, WordClass wordClass) throws IOException {
		this.wordClass = wordClass;

		switch (wordClass) {
			case ARTICLES:
				wordsToRemove = articles;
				break;
			case PRONOUNS:
				wordsToRemove = prepositions;
				break;
			case RANDOM:
				break;
		}

		Document doc = Jsoup.connect(url).get();

		Elements paragraphs = doc.select("p");
		for (Element paragraph : paragraphs) {
			String changedText = changeText(paragraph.text());
			paragraph.text("");
			boolean firstTime = true;
			for (String part : changedText.split(PLACE_HOLDER)) {
				if (part.isEmpty()) {
					continue;
				}
				int begin = part.indexOf("<");
				int end = part.indexOf(">");
				if (begin < 0 || end < 0) {
					continue;
				}
				String tmp = part.substring(begin+1, end);
				int number = Integer.parseInt(tmp);
				part = part.substring(end+1);
				if (!firstTime) {
					paragraph.append("<span id=\"" + number + "\">" + getSelect().toString() + "</span>");
				}
				else {
					firstTime = false;
				}
				paragraph.append(part);
			}
		}

		Elements links = doc.select("link[rel='stylesheet']");
		for (Element link : links) {
			String href = link.attr("href");
			link.attr("href", PROTOCOL + COUNTRY_CODE_GERMANY + BASE_PATH + href);
		}

		return doc.outerHtml();
	}

	private void initSets() {
		articles.put(new Integer(0), "der");
		articles.put(new Integer(1), "die");
		articles.put(new Integer(2), "das");
		articles.put(new Integer(3), "ein");
		articles.put(new Integer(4), "eine");
		articles.put(new Integer(5), "Der");
		articles.put(new Integer(6), "Die");
		articles.put(new Integer(7), "Das");
		articles.put(new Integer(8), "Ein");
		articles.put(new Integer(9), "Eine");

		prepositions.put(new Integer(0), "in");
		prepositions.put(new Integer(1), "an");
		prepositions.put(new Integer(2), "auf");
		prepositions.put(new Integer(3), "vor");
		prepositions.put(new Integer(4), "hinter");
		prepositions.put(new Integer(5), "über");
		prepositions.put(new Integer(6), "unter");
		prepositions.put(new Integer(7), "nach");
		prepositions.put(new Integer(8), "zu");
		prepositions.put(new Integer(9), "In");
		prepositions.put(new Integer(10), "An");
		prepositions.put(new Integer(11), "Auf");
		prepositions.put(new Integer(12), "Vor");
		prepositions.put(new Integer(13), "Hinter");
		prepositions.put(new Integer(14), "Über");
		prepositions.put(new Integer(15), "Unter");
		prepositions.put(new Integer(16), "Nach");
		prepositions.put(new Integer(17), "Zu");
	}

	private void setSelect(String select) {
		this.buf = select;
	}

	private String getSelect() {
		if (wordClass == WordClass.RANDOM) {
			return buf;
		}

		StringBuffer buf = new StringBuffer();
		buf.append("<select>");
		for (Entry<Integer, String> entry : wordsToRemove.entrySet()) {
			buf.append("<option value=\"" + entry.getKey() + "\">" + entry.getValue() + "</option>");
		}
		buf.append("</select>");

		return buf.toString();
	}

	private String changeText(String text) {
		if (wordClass == WordClass.RANDOM) {
			return changeTextRandom(text);
		}
		return changeTextArticlesAndPronouns(text);
	}

	private String changeTextRandom(String text){
		List<String> wordList = new ArrayList<String>();
		Map<Integer, String> removedWords = new HashMap<Integer, String>();
		for (String word : text.split(" ")) {
			wordList.add(word);
		}

		int size = wordList.size();
		for (int i=0; i<10; i++) {
			int random = randomNumber(size);
			String wordToRemove = wordList.get(random);
			if (wordToRemove.startsWith(PLACE_HOLDER)) {
				continue;
			}
			wordList.set(random, PLACE_HOLDER + "<" + i + ">");
			removedWords.put(new Integer(i), wordToRemove);
		}

		StringBuffer buf = new StringBuffer();
		buf.append("<select>");
		for (Entry<Integer, String> entry: removedWords.entrySet()) {
			buf.append("<option value=\"" + entry.getKey() + "\">" + entry.getValue() + "</option>");
		}
		buf.append("</select>");
		setSelect(buf.toString());

		buf = new StringBuffer();
		boolean firstTime = true;
		for (String word : wordList) {
			if (!firstTime) {
				buf.append(" ");
			}
			else {
				firstTime = false;
			}
			buf.append(word);
		}
		return buf.toString();
	}

	private String changeTextArticlesAndPronouns(String text) {
		StringBuffer buf = new StringBuffer();
		boolean firstTime = true;
		for (String word : text.split(" ")) {
			if (!firstTime) {
				buf.append(" ");
			}
			else {
				firstTime = false;
			}
			boolean found = false;
			for (Entry<Integer, String> entry : wordsToRemove.entrySet()) {
				if (entry.getValue().equals(word)) {
					found = true;
					buf.append(PLACE_HOLDER + "<" + entry.getKey() + ">");
				}
			}
			if (!found) {
				buf.append(word);
			}
		}
		return buf.toString();
	}

	/**
	 * Calculates a random number between zero and max exclusively.
	 *
	 * @param max - the maximum the random number can reach.
	 * @return the random number.
	 */
	private int randomNumber(int max) {
		Random randomizer = new Random();
		int randomNumber = randomizer.nextInt(max);

		return randomNumber;
	}
}