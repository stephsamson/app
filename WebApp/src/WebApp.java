import java.util.HashMap;

import com.sun.tools.internal.ws.processor.model.Model;
import org.eclipse.jetty.webapp.Configuration;
import spark.ModelAndView;
import spark.template.mustache.MustacheTemplateEngine;

import static spark.Spark.*;


public class WebApp {
    public static void main(String[] args) {
        // Configure Spark
        port(4242); // http://localhost:4242/
        staticFiles.location("/public"); // HTML & CSS files

        // Routes
        get("/wiki", (request, response) -> {
            response.status(200);
            response.type("text/html");

            Parser parser = new Parser();
            String url = request.queryParams("url");
            String cloze = request.queryParams("cloze").toUpperCase();

            return parser.processSite(url, WordClass.valueOf(cloze));
        });

        get("/", (request, response) -> {
            return new ModelAndView(new HashMap(), "index.html");
        }, new MustacheTemplateEngine());
    }
}
